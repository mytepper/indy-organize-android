package coderocks.ticketmhee.Adaptor;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import coderocks.ticketmhee.R;

/**
 * Created by likit on 7/18/2017 AD.
 */

public class RecyclerAdaptor extends RecyclerView.Adapter<RecyclerAdaptor.ItemViewHolder> {


    private JsonArray items;
    private LayoutInflater inflater;

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView tvName;
        TextView tvTel;

        public ItemViewHolder(View view) {
            super(view);

            tvName = (TextView)view.findViewById(R.id.tv_name);
            tvTel = (TextView)view.findViewById(R.id.tv_tel);
        }



    }

    public RecyclerAdaptor(JsonArray items) {
        this.items = items;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        View v  = inflater.inflate(R.layout.itemlistview,parent,false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, final int position) {
        JsonObject obj = items.get(position).getAsJsonObject();
        holder.tvName.setText(obj.get("name").toString());
        holder.tvTel.setText(obj.get("phone").toString());

    }



    @Override
    public int getItemCount() {
        return items.size();
    }
}