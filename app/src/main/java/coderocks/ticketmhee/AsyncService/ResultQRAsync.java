package coderocks.ticketmhee.AsyncService;

import android.os.AsyncTask;
import android.util.Log;

import coderocks.ticketmhee.Manager.HttpManager;
import coderocks.ticketmhee.Model.QRcodeResponse;
import coderocks.ticketmhee.Model.QRcodeResponseData;
import coderocks.ticketmhee.Util.Util;


/**
 * Created by likit on 18-Jan-17.
 */

public class ResultQRAsync extends AsyncTask<Long, Void, Void> {

    private int userID;

    public interface AsyncCallback {
        void OnSuccess(QRcodeResponseData user);

        void OnError(String message);
    }

    public ResultQRAsync(int userID) {
        this.userID = userID;
    }

    private AsyncCallback callback;

    @Override
    protected Void doInBackground(Long... params) {

        retrofit2.Call<QRcodeResponse> call = HttpManager.getInstance()
                .getService()
                .getUserInfo(userID);

        call.enqueue(new retrofit2.Callback<QRcodeResponse>() {
            @Override
            public void onResponse(retrofit2.Call<QRcodeResponse> call, retrofit2.Response<QRcodeResponse> response) {
                if (response.isSuccessful()) {
                    QRcodeResponse resp = response.body();
                    Log.d("ResultQRAsync","QRcodeResponse : " + resp);
                    callback.OnSuccess(resp.getData().get(0));
                } else {
                    callback.OnError(Util.getErrorMessage(response.errorBody()));
                }
            }

            @Override
            public void onFailure(retrofit2.Call<QRcodeResponse> call, Throwable t) {
                callback.OnError(t.toString());
            }
        });

        return null;
    }

    public void setCallback(AsyncCallback callback) {
        this.callback = callback;
    }

}
