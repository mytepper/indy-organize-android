package coderocks.ticketmhee;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        Button qrcodescanBtn = (Button) findViewById(R.id.qrcodescanBtn);
        Button buttonDone = (Button) findViewById(R.id.button_done);

        qrcodescanBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(v.getContext(), QrcodeScan.class);

                v.getContext().startActivity(intent);

            }
        });
        final EditText button2 = (EditText)findViewById(R.id.button2);
       /* button2.setFocusableInTouchMode(true);
        button2.requestFocus();
        button2.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    Log.d("test","==>");
                    return true;
                }
                return false;
            }
        });*/
        button2.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    Log.d("test","==>"+button2.getText().toString());

                    /*
                    Intent intent = new Intent(v.getContext(), OrganizeAcitvity.class);
                    Bundle b = new Bundle();
                    b.putString("ticketid", button2.getText().toString());
                    intent.putExtras(b);
                    v.getContext().startActivity(intent);
                    */
                    Intent intent = new Intent(v.getContext(), CheckList.class);
                    Bundle b = new Bundle();
                    b.putString("ticketid", button2.getText().toString());
                    intent.putExtras(b);
                    v.getContext().startActivity(intent);

                    return true;
                }
                return false;
            }
        });

        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ticketId = button2.getText().toString();
                if (!ticketId.isEmpty()) {
                    startActivity(new Intent(MainActivity.this, CheckList.class).putExtra("ticketid", button2.getText().toString()));
                } else {
                    Toast.makeText(MainActivity.this, "กรุณาเพิ่ม Event Code", Toast.LENGTH_SHORT).show();
                }
            }
        });




    }

}
