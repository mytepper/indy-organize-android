package coderocks.ticketmhee.Manager.HTTP;

import coderocks.ticketmhee.Model.QRcodeResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by likit on 17/7/2560.
 */

public interface QRService {

    @FormUrlEncoded
    @POST("checkQrCode")
    Call<QRcodeResponse> getUserInfo(@Field("id") int userID);


}
