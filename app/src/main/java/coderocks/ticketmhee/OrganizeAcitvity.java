package coderocks.ticketmhee;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

/**
 * Created by watcharatep.i on 5/18/2016 AD.
 */
public class OrganizeAcitvity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.organiz);
        Bundle extras = getIntent().getExtras();
        final String id = extras.getString("ticketid");


        final TextView title = (TextView)findViewById(R.id.textView8);

        final TextView date = (TextView)findViewById(R.id.textView7);


        final ImageView savebtn = (ImageView)findViewById(R.id.savebtn);


        savebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getBaseContext(), CheckList.class);
                intent.putExtra("ticketid", ""+id);
                intent.putExtra("title", title.getText().toString());
                startActivity(intent);
            }
        });
    /*

        if(id.equals("1")){
            title.setText("เทคนิคการฉีดพลาสติก พื้นฐาน รุ่นที่ 10");
            date.setText("วันที่ 23-25 พฤษภาคม 2559");
        }else if(id.equals("2")){
            title.setText("เสกสรรค์ปั้นตลาด เสริมมูลค่าเพิ่ม ด้วยนวัตกรรมบรรจุภัณฑ์");
            date.setText("วันที่ 23-25 พฤษภาคม 2559");
        }else if(id.equals("3")){
            title.setText("หลักสูตร ED04");
            date.setText("วันที่ 26-27 พฤษภาคม 2559");
        }else if(id.equals("4")){
            title.setText("หลักสูตรสัมมนา-Reed  Tradex-Thailand Industrial Expo");
            date.setText("วันที่ 23 มิถุนายน 2559");
        }else if(id.equals("5")){
            title.setText("ความร่วมมือในการพัฒนาอุตสาหกรรมการผลิตชิ้นส่วนยานยนต์ในกลุ่มประเทศอาเซียน");
            date.setText("วันที่ 11 พฤษภาคม 2559");
        }else if(id.equals("6")){
            title.setText("ศูนย์ทดสอบยานยนต์และชิ้นส่วน ความสำคัญใน Auto Technopolis ของประเทศไทย");
            date.setText("วันที่ 14 พฤษภาคม 2559");
        }else if(id.equals("7")){
            title.setText("การชุบแข็งชิ้นส่วนยานยนต์และเครื่องจักรกล");
            date.setText("วันที่ 11-14 พฤษภาคม 2559");
        }

        */


        Ion.with(getBaseContext())
                .load("http://ticketmhee.com/api/index.php/api/getTicketAllByEventID")
                .setBodyParameter("id",id)
        // .setBodyParameter("data","{\"customer\":{\"href\":\"https://hydra.unicity.net/v5/customers?id.unicity=108357166\"},\"market\":\"TH\",\"timeout\":\"600\"}")
                .asJsonObject()

                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {

                        try{
                            JsonArray ar = result.get("data").getAsJsonArray();
                            JsonObject obj = ar.get(0).getAsJsonObject();
                            title.setText(obj.get("name").getAsString());
                            date.setText(obj.get("sale_start_date").getAsString()+" - "+obj.get("sale_end_date").getAsString());
                        }catch (Exception e1){

                        }
                        Log.d("dev","===>"+result);

                        //JsonArray ar = result.get("data").getAsJsonArray();

                        //JsonObject obj = ar.get(0).getAsJsonObject();



                       // title.setText("เทคนิคการฉีดพลาสติก พื้นฐาน รุ่นที่ 10");
                        // do stuff with the result or error

                       /* try {
                            int error =  result.get("error").getAsJsonObject().get("code").getAsInt();
.
                            if(error>0){
                               /// hubstatus.setText("HUBSPOKE ERROR");
                               // hubstatus.setTextColor(Color.RED);
                            }
                        }catch (Exception ex1){
                           // hubstatus.setText("COMPLETE");
                           // hubstatus.setTextColor(Color.BLUE);
                        }*/

                        Log.d("dev","===="+result);


                    }
                });






    }
}
