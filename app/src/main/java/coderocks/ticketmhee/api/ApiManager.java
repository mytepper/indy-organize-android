package coderocks.ticketmhee.api;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import coderocks.ticketmhee.Util.Constant;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by oob on 28/12/2558.
 */
public class ApiManager {

    public static ApiServices getService(Context context) {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit restAdapter = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(Constant.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        return restAdapter.create(ApiServices.class);
    }

}
