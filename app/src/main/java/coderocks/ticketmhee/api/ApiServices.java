package coderocks.ticketmhee.api;

import coderocks.ticketmhee.Model.checkStatus.CheckStatusData;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by oob on 28/12/2558.
 */
public interface ApiServices {

    @FormUrlEncoded
    @POST("checkStatus")
    Call<CheckStatusData> checkQRCode(
            @Field("id") String id
    );

}
