package coderocks.ticketmhee.Util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;


public class Util {

    private static Gson gson;

    public static Gson createGson() {
        if (gson == null) {
            gson = new GsonBuilder()
                    .setDateFormat(Constant.JSON_DATE_TIME_FORMAT)
                    .create();
        }
        return gson;
    }


    public static String getErrorMessage(ResponseBody errorBody) {
        try {
            JSONObject jsonErrorBody = new JSONObject(errorBody.string());
            return jsonErrorBody.get("OnError").toString();
        } catch (IOException e) {
            return e.getMessage();
        } catch (JSONException e) {
            return e.getMessage();
        }
    }


}
