package coderocks.ticketmhee.Util;

/**
 * Created by likit on 17/07/2017.
 */
public class Constant {

    // static String BASE_API_URL = "http://www.mcr-team.com/communicate/api/";
    //public static String BASE_API_URL = "http://192.168.1.102/communicate/api/";
    //public static String BASE_API_URL = "http://192.168.0.115/communicate/api/";
    public static String BASE_API_URL = "http://ticketmhee.com/api/index.php/api/";
    public static String PROFILE_IMAGE_URL = BASE_API_URL + "upload/profile/";

    public static String JSON_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static String OAUTHEN_CLIENT = "784056543803-t650apr7or59p0m9cge8r6u77vqb3ts2.apps.googleusercontent.com";

    // play store 784056543803-t650apr7or59p0m9cge8r6u77vqb3ts2.apps.googleusercontent.com //
    // mac : 747399216417-pmgfe4pe2kgbhitra2qk7jeivnkkan5g.apps.googleusercontent.com 

    public static int MAP_ZOOM = 15;
    public static float MAX_IMAGE_SIZE = 300;

    public static final int REQUEST_CODE_PICKER_PROFILE = 100;
    public static final int REQUEST_CODE_PICKER_EMERGENCY = 101;
    public static final int REQUEST_CODE_SEARCH_EMERGENCY = 102;
    public static final int EMERGENCY_REQUEST_CODE = 103;
    public static final int REQUEST_PLACE_PICKER = 104;
    public static final int REQUEST_GOOGLE_SIGN_IN = 105;

}

