package coderocks.ticketmhee;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import coderocks.ticketmhee.AsyncService.ResultQRAsync;
import coderocks.ticketmhee.Model.QRcodeResponseData;
import coderocks.ticketmhee.Model.checkStatus.CheckStatusData;
import coderocks.ticketmhee.api.ApiManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResultActivity extends AppCompatActivity {

    private QRcodeResponseData data;
    private TextView textview, resultTxt, holderTxt, tickettypeTxt, emailTxt, phoneTxt;
    private ImageView imageView;

    private String ticketId;
    private String id;

    private ProgressDialog progressDialog;
    private AlertDialog errorAlert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ticket);

        Bundle extras = getIntent().getExtras();
        id = extras.getString("id");

        //findviewbyid
        //textview = (TextView) findViewById(R.id.textview);
        resultTxt = (TextView) findViewById(R.id.resultTxt);
        holderTxt = (TextView) findViewById(R.id.holderTxt);
        tickettypeTxt = (TextView) findViewById(R.id.tickettypeTxt);
        emailTxt = (TextView) findViewById(R.id.emailTxt);
        phoneTxt = (TextView) findViewById(R.id.phoneTxt);

        imageView = (ImageView) findViewById(R.id.icon_success);

        //textview.setText(id);

        getUserInfo(id);

        TextView savetext = (TextView) findViewById(R.id.savetext);
        savetext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkStatus(ticketId);
            }
        });
    }

    // Professor
    private void checkStatus(String ticket_id){
        showProgress(true);
        Call<CheckStatusData> call = ApiManager.getService(this).checkQRCode(
                ticket_id
        );
        call.enqueue(new Callback<CheckStatusData>() {
            @Override
            public void onResponse(@NonNull Call<CheckStatusData> call, @NonNull Response<CheckStatusData> response) {
                showProgress(false);
                CheckStatusData data = response.body();

                Intent intent = new Intent(ResultActivity.this, CheckList.class);
                Bundle b = new Bundle();
                b.putString("ticketid", id);
                intent.putExtras(b);
                startActivity(intent);

                if(data!=null){
                    if(data.status){
                        Log.i("ResultActivity","success");
                    } else {
                        Log.i("ResultActivity","fail");
                    }
                } else {
                    Log.i("ResultActivity","error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<CheckStatusData> call, @NonNull Throwable t) {
                showProgress(false);
                Log.i("ResultActivity","server error");

                Intent intent = new Intent(ResultActivity.this, CheckList.class);
                Bundle b = new Bundle();
                b.putString("ticketid", id);
                intent.putExtras(b);
                startActivity(intent);
            }
        });
    }

    public void getUserInfo(String id) {
        ResultQRAsync resultQRAsync = new ResultQRAsync(Integer.parseInt(id));
        resultQRAsync.setCallback(new ResultQRAsync.AsyncCallback() {
            @Override
            public void OnSuccess(QRcodeResponseData user) {
                data = user;

                ticketId = data.getTicket_id();

                init();

                //resultTxt.setText(data.getStatus());
                //holderTxt.setText(data.getName());
                //tickettypeTxt.setText(data.getType());
                //emailTxt.setText(data.getEmail());
                //phoneTxt.setText(data.getPhone());
                //resultTxt.setText(""+data.getStatus());
            }

            @Override
            public void OnError(String message) {
                errorAlert = new AlertDialog.Builder(ResultActivity.this)
                        .setTitle("Indy Organize")
                        .setMessage("ไม่พบข้อมูล")
                        .setPositiveButton("เสร็จ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                //startActivity(new Intent(LoginActivity.this, MainProfessorActivity.class).putExtra("USER_NAME",userName));
                                finish();
                                //overridePendingTransition(R.anim.in_right_to_left, R.anim.out_right_to_left);
                            }
                        })
                        .setCancelable(false)
                        .create();
                errorAlert.show();
            }
        });
        resultQRAsync.execute();

    }

    private void showProgress(boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (show) {
            progressDialog = ProgressDialog.show(this, "Please wait.",
                    "Loading..", true);
        } else {
            try{
                progressDialog.dismiss();
            } catch(Exception error){
                Log.e("ResultActivity", "Fail: ", error);
            }
        }
    }

    private void init() {
        // set view value
        //textview.setText(data.getEmail());
        resultTxt.setText(data.getStatus());
        holderTxt.setText(data.getName());
        tickettypeTxt.setText(data.getType());
        emailTxt.setText(data.getEmail());
        phoneTxt.setText(data.getPhone());
        if (data.getStatus().equals("true")) {
            imageView.setImageResource(R.drawable.icon_success);
        } else {
            imageView.setImageResource(R.drawable.icon_failed);
        }
    }
}
