package coderocks.ticketmhee.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by likit on 7/17/2017 AD.
 */

public class QRcodeResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private List<QRcodeResponseData> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<QRcodeResponseData> getData() {
        return data;
    }

    public void setData(List<QRcodeResponseData> data) {
        this.data = data;
    }
}
