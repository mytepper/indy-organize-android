package coderocks.ticketmhee;

import android.app.Application;

import coderocks.ticketmhee.Manager.Contextor;


/**
 * Created by chawdee on 20/04/2016.
 */
public class MainApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();

		// Initialize thing(s) here
		Contextor.getInstance().init(getApplicationContext());

	}

	@Override
	public void onTerminate() {
		super.onTerminate();
	}
}
