package coderocks.ticketmhee;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import coderocks.ticketmhee.Adaptor.RecyclerAdaptor;


public class CheckList extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerAdaptor listAdapter;
    private String id;
    private String title;
    private DonutProgress donut_progress;
    private TextView totalCheckin;
    private TextView totalRemain;
    private SwipeRefreshLayout swipeRefreshLayout;

    TextView textview;

    private AlertDialog errorAlert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.total);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        Bundle b = getIntent().getExtras();
        id = b.getString("ticketid");
        //title = b.getString("title");
        //title = "XXXX";


        //ArrayList<User> arrayOfUsers = new ArrayList<User>();
        // Create the adapter to convert the array to views
        // final UsersAdapter adapter = new UsersAdapter(this, arrayOfUsers);


        textview = (TextView) findViewById(R.id.textview);


        // Attach the adapter to a ListView
        //ListView listView = (ListView) findViewById(R.id.lvItems);
        // listView.setAdapter(adapter);

        totalCheckin = (TextView) findViewById(R.id.totalCheckin);
        totalRemain = (TextView) findViewById(R.id.totalRemain);
        donut_progress = (DonutProgress) findViewById(R.id.donut_progress);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }
        });
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                refresh();
            }
        });


        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CheckList.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });


    }

    private void refresh() {
        Log.i("Result","result refresh");
        swipeRefreshLayout.setRefreshing(true);
        Ion.with(getBaseContext())
                .load("http://ticketmhee.com/api/index.php/api/checkEventCode")
                .setBodyParameter("id", id)
                // .setBodyParameter("data","{\"customer\":{\"href\":\"https://hydra.unicity.net/v5/customers?id.unicity=108357166\"},\"market\":\"TH\",\"timeout\":\"600\"}")
                .asJsonObject()

                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        Log.i("Jay Log","Success : " + result.get("success").getAsBoolean());
                        if (result.get("success").getAsBoolean()) {
                            //Log.i("Result","result : " + result);
                            totalCheckin.setText(result.get("checked").getAsString());
                            totalRemain.setText(result.get("tickets").getAsString());
                            textview.setText(result.get("title").getAsString());
                            //textview.setText("Title");
                            /*
                            String ticTitle = result.get("title").getAsString();

                            if (!ticTitle.isEmpty()) {
                                textview.setText(ticTitle);
                            } else {
                                textview.setText("");
                            }
                            */

                            JsonArray data = result.getAsJsonArray("data");
                            if (data != null) {
                                listAdapter = new RecyclerAdaptor(data);
                                recyclerView.setAdapter(listAdapter);
                            }

                            swipeRefreshLayout.setRefreshing(false);

                        } else {
                            /*
                            errorAlert = new AlertDialog.Builder(CheckList.this)
                                    .setTitle("Indy Organize")
                                    .setMessage("ไม่พบ Event Code")
                                    .setPositiveButton("เสร็จ", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            //startActivity(new Intent(LoginActivity.this, MainProfessorActivity.class).putExtra("USER_NAME",userName));
                                            finish();
                                            //overridePendingTransition(R.anim.in_right_to_left, R.anim.out_right_to_left);
                                        }
                                    })
                                    .setCancelable(false)
                                    .create();
                            errorAlert.show();
                            */
                            startActivity(new Intent(CheckList.this,MainActivity.class));
                        }
//                        try {
//
//                            // JSONObject issueObj = new JSONObject(result.get("data").getAsString());
//
//                            Log.d("dev", "===>" + result.get("data").getAsJsonObject().get("name"));
//
//
//                        } catch (Exception e1) {
//
//                            Log.d("dev", "size===>" + e1);
//                        }

                    }
                });
    }

/*
    public class UsersAdapter extends ArrayAdapter<User> {
        public UsersAdapter(Context context, ArrayList<User> users) {
            super(context, 0, users);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            User user = getItem(position);
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.items, parent, false);
            }
            // Lookup view for data population
            TextView tvName = (TextView) convertView.findViewById(R.id.cname);

            tvName.setText(user.name);
            return convertView;
        }
    }

    public class User {
        public String name;

        public User(String name) {
            this.name = name;
        }
    }
    */
}
